""" C in MVC, main script, the controller of the program itself """
import diary_model
from diary_view import CustomCalendarWidget
import sys
import datetime
from PyQt5.QtGui import QKeySequence
from PyQt5.QtCore import QDate, Qt
from PyQt5.QtWidgets import QShortcut, QAction, QApplication, QMainWindow, QTextEdit, QPushButton, QVBoxLayout, \
    QHBoxLayout, QWidget, QCalendarWidget, QLabel
from config import *

class DearDiaryApp(QMainWindow):
    def __init__(self) -> None:
        super().__init__()
        self._date = datetime.date.today().strftime("%Y-%m-%d")
        self._weekday = WEEKDAY_NAME[datetime.date.today().weekday()]
        self.init_ui()
        
    def init_ui(self) -> None:
        """
        input: self
        do 
        1) init windows
        2) init widgets, configure them
        3) apply widgets to window
        4) update on other window feature
        return: none
        """
        # set windows base param
        self.setWindowTitle('Ms Diary')
        self.setGeometry(250, 200, 1300, 700)
        self.setFixedSize(self.size())

        # Initiate widgets
        self.calendar = CustomCalendarWidget(self)
        self.title_label = QLabel(DIARY_INPUT_BOX_HEADER.format(self._date, self._weekday), self)
        self.text_edit = QTextEdit(self)
        self.text_edit_show = QTextEdit(self)
        self.submit_button = QPushButton('Thats it!', self)
        left_layout  = QVBoxLayout()
        mainlayout = QHBoxLayout()
        central_widget  = QWidget(self)

        # configure widget param
        self.calendar.setVerticalHeaderFormat(QCalendarWidget.NoVerticalHeader)
        self.calendar.setGeometry(150, 150, 50, 50)
        self.calendar.setGridVisible(True)
        self.calendar.selectionChanged.connect(self.cb_show_diary_record)
        
        self.text_edit_show.setReadOnly(True)

        self.submit_button.clicked.connect(self.cb_submit_diary)
        
        left_layout.addWidget(self.calendar)
        left_layout.addWidget(self.title_label)
        left_layout.addWidget(self.text_edit)
        left_layout.addWidget(self.submit_button)

        mainlayout.addLayout(left_layout)
        mainlayout.addWidget(self.text_edit_show)

        # apply widget
        central_widget .setLayout(mainlayout)
        self.setCentralWidget(central_widget)
        
        # update on other window feature
        self.calendar.diary_date_list = self.diary_qdate_list
        self.cb_show_diary_record()

        self.quick_submit_hotkey_action = QAction(self)
        self.quick_submit_hotkey_action.setShortcut(QKeySequence("Ctrl+R"))  # Define the hotkey (Ctrl+R)
        self.quick_submit_hotkey_action.triggered.connect(self.cb_submit_and_close)
        self.addAction(self.quick_submit_hotkey_action)  
        self.text_edit.setFocus()

    def cb_submit_diary(self) -> None:
        """
        input: none
        do
        1) read the text inputed in the text box
        2) if it is not empty, create a diary object, input the diary, record the diary
        3) update the diary date
        return: none  
        """
        entered_text = self.text_edit.toPlainText()
        if entered_text != '': 
            diary = diary_model.Diary()
            diary(entered_text)
            diary_model.record_diary(diary)
            self.text_edit.clear()
            self.calendar.diary_date_list = self.diary_qdate_list
            self.cb_show_diary_record()

    def cb_show_diary_record(self) -> None:
        """
        input: none
        do
        1) read todays date as a string 
        2) query it with the date
        3) show the diary at the right text box
        return: none  
        """
        # get today date
        date = self.calendar.selectedDate().toString("yyyy-MM-dd")

        # query the diary record
        diary_record = diary_model.query_diary(date=date)

        # prepare the diary string
        diary_string = DIARY_OUTPUT_BOX_HEADER.format(date)
        for row in diary_record:
            diary_string += DIARY_OUTPUT_BOX_BODY.format(row[2], row[4]) # row[2] = time, row[4] = diary
        
        # show the diary
        self.text_edit_show.setPlainText(diary_string)

    def cb_submit_and_close(self) -> None:
        """
        do
        1) call submit function, and close windows  
        """
        self.cb_submit_diary()
        exit(0)

    @property
    def diary_qdate_list(self) -> list:
        """
        input: none
        do
        1) query the date in diary record db and find distinct date
        2) record down as a date list
        return: the list of date  
        """ 
        return [QDate.fromString(date, "yyyy-MM-dd") for date in diary_model.query_diary_date_distinct()]

if __name__ == '__main__':
    if len(sys.argv) > 1: diary_model.clear_all_diary()
    else:
        app = QApplication(sys.argv)
        msdiary = DearDiaryApp()
        msdiary.show()
        sys.exit(app.exec_())
else:
    print(f"ERROR! {__file__} should be executed as main!")
    exit(2)