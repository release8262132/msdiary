""" 
Configure py file to include macro defination
"""
# v1.1 TODO
# do some simple styling
# wirte simple user manual
# research qsqlite and try integrate
# 

# v1.2 TODO
# feature: add clear record feature
# feature: add edit diary feature
# feature: add prompt diary txt feature
# installation management, database management


# Wishlist
# add autentication approach
# cross platform server

# file paths
DAIRY_TXT_FILE_PATH = 'diaryrecord.txt'
DAIRY_DB_FILE_PATH = 'diaryrecord.db'

# 0 - 7
WEEKDAY_NAME = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

# TXT 
TXT_HEADER = \
'                          ----    MY DIARY    ----                          \n\
-------------------------------------------------------------------------------'

TXT_DIARY_HEADER = \
'\
\n\
\n{}                   {}\
'
TXT_DIARY_BODY = \
'\
\n-- {}:\
\n  {}\
'
# DB

# UI

markColor = [255,0,0]

DIARY_INPUT_BOX_HEADER = \
"{}\
                                                                            \
                                                                           {}\
\nDear Ms Diary:"

DIARY_OUTPUT_BOX_HEADER = \
'                                                 DIARY ON {}\
\n------------------------------------------------------------------------------------------------------------'

DIARY_OUTPUT_BOX_BODY = \
'\
\n\n----> {}\
\n{}\
'