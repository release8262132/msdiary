""" V in MVC, provide ui interface function """
from PyQt5.QtGui import QColor
from PyQt5.QtCore import QDate, QPoint
from PyQt5.QtWidgets import QCalendarWidget
from config import *

if __name__ == '__main__': exit(2) 

class CustomCalendarWidget(QCalendarWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.diary_date_list = []

    def paintCell(self, painter, rect, date):
        painter.save()
        
        super().paintCell(painter, rect, date)
        
        if date == QDate.currentDate(): # paint light blue for current date
            painter.fillRect(rect, QColor(173, 216, 230, 95))  
        
        if date in self.diary_date_list: # write dot fot date that have diary
            painter.setBrush(QColor(markColor[0], markColor[1], markColor[2]))
            painter.drawEllipse(rect.topLeft() + QPoint(12, 7), 3, 3)
            
        painter.restore()