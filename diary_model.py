""" M in MVC, provide only db related processing """
from typing import Any
import sqlite3
import datetime
from config import *

if __name__ == '__main__': exit(2) 

""" TODO 
Need to have a general to call function that validate the legitimate of db and txt, and a funct to create a legit one 
Validate db with inner checking and md5 
Validate the txt
"""
class Diary:
    """ Diary object
    >>> diary = Diary()
    >>> diary() # return the all infomation of this object
    >>> diary({diary<str>}) # set the diary, it will automatically record the datetime info, and return all infomation of this object
    >>> diary.len_diary # return the len of the diary
    >>> diary # return the string of diary 
    """
    def __init__(self) -> None:
        self._diary = ''
        self._date = ''
        self._time = ''
        self._weekday = ''
    
    # when Diary object is called(), return dict of all information, when args is passed, record it as diary
    def __call__(self, *args: Any) -> Any: 
        if args: self.diary = args[0]
        return {"Date": self._date, "Time": self._time, "Weekday": self._weekday, "Diary": self._diary, "Length": self.len_diary}

    # when Diary object is printed, print diary 
    def __str__(self) -> str: return self._diary

    @property # decorate len_diary as a property will make it a variable that always equal to the output of this function
    def len_diary(self) -> int: return len(self.diary)

    @property
    def diary(self) -> None: return self._diary

    @diary.setter
    def diary(self, value:str) -> None: 
        """ update time, date, weekday when this diary is entered """
        self._date = datetime.date.today().strftime("%Y-%m-%d")
        self._time = datetime.datetime.now().today().strftime("%H:%M:%S")
        self._weekday = WEEKDAY_NAME[datetime.date.today().weekday()]
        self._diary = value


""" API with datebase """
def _last_update_is_today(db:sqlite3.Cursor, diary_info:dict) -> bool: 
    """
    input: db cursor, diary_info
    do
    1) query the lastupdate date and compare with the diary input date
    output: boolean  
    """
    return db.execute("SELECT date from lastupdate").fetchone()[0] == diary_info["Date"]

def record_diary(diary:Diary) -> None:
    """ 
    input: diary obj
    do:
    0) input validation
    1) insert a row into local db
    2) append the local txt file
    3) close all and return success
    return: none
    """
    # read diary info
    diary_info = diary()

    # open db 
    conn = sqlite3.connect(DAIRY_DB_FILE_PATH)
    diary_db = conn.cursor() # open or create a database

    # insert into db
    diary_db.execute(f"INSERT INTO diary VALUES(\'{diary_info['Date']}\', \'{diary_info['Weekday']}\', \'{diary_info['Time']}\', \'{diary_info['Length']}\', \'{diary_info['Diary']}\')")

    # prepare diary txt input
    diary_txt_input = TXT_DIARY_HEADER.format(diary_info["Date"], diary_info["Weekday"]) if not _last_update_is_today(diary_db, diary_info) else ''
    diary_txt_input += TXT_DIARY_BODY.format(diary_info["Time"], diary_info["Diary"])

    # open txt
    diary_txt = open(DAIRY_TXT_FILE_PATH, "a")
    
    # append into txt
    diary_txt.write(diary_txt_input)

    # update
    diary_db.execute(f"UPDATE lastupdate SET date = \'{diary_info['Date']}\'")
    conn.commit()

def clear_all_diary() -> None:
    """
    Input: None
    do: 
    1) clear all db row, reset the last update to 1900, preserve the table and column name
    2) clear txt except first two rows
    """
    # open txt 
    # clear txt and reset
    with open(DAIRY_TXT_FILE_PATH, 'w') as file: file.writelines(TXT_HEADER)

    # Open db
    conn = sqlite3.connect(DAIRY_DB_FILE_PATH)
    diary_db = conn.cursor() 

    # Reset db
    diary_db.execute(f"UPDATE lastupdate SET date = \'1900-01-01\'")
    diary_db.execute(f"DELETE FROM diary")
    conn.commit()

def query_diary(**kwarg) -> list:
    """
    Input: date, ?time_min, ?time_max, ?Weekday
    do
    1) use the constraint inputed to query diary
    Output: List of dict which contain every needed info of the row
    """
    # open db
    conn = sqlite3.connect("diaryrecord.db")
    diary_db = conn.cursor()

    # query db
    query = 'SELECT * FROM diary'
    if kwarg and kwarg["date"]: query += f' WHERE date = \'{kwarg["date"]}\' '

    return diary_db.execute(query).fetchall()

def query_diary_date_distinct() -> list:
    """
    Input: None
    do
    1) query all distinct date, for coloring purpose
    Output: list of the date   
    """
    # open db
    conn = sqlite3.connect("diaryrecord.db")
    diary_db = conn.cursor()

    # query db
    query = 'SELECT DISTINCT date from diary'

    return [date[0] for date in diary_db.execute(query).fetchall()]

